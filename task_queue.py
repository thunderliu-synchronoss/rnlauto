#!/usr/bin/python
# coding=utf-8

from queue import Queue

"""
Each load of this module will result in an instance of Queue
Put the task_queue into single module to decouple from suppliers and slaves in gen_mails
"""

task_queue = Queue()
def get_task_queue():
	return task_queue