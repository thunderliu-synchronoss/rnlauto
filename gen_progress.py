#!/usr/bin/env python
# coding=utf-8

from monitor.progress import Progress
from config import progress


if __name__ == '__main__':
	p = Progress(**progress)
	p.progress_all()
