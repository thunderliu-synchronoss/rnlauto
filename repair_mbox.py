#!/usr/bin/env python
# coding=utf-8

import sys,time

from monitor.remote import SSHRunner
from config import remote_jobs



def loop():
	def_user = remote_jobs['defaults']['conn_user']
	def_pwd = remote_jobs['defaults']['conn_pwd']
	task_user = remote_jobs['defaults']['task_user']
	repair_host = remote_jobs['repair']['host']

	ssh = SSHRunner()
	ssh.connect(def_user, def_pwd, repair_host)
	ch = ssh.shell()

	ch.send("stty -echo\n")  # close echo of input

	ch.send("su - %s\n" % task_user)
	while not ch.recv_ready():
		time.sleep(0.5)
	print(ch.recv(1024).decode("utf-8"))

	ch.send("\n")
	while not ch.recv_ready():
		time.sleep(0.5)
	echo = ch.recv(1024).decode("utf-8")

	for mail in sys.stdin:
		mail = mail.strip('\r\n')
		repair_single(ch, mail, echo)

	ch.close()
	ssh.close()



def repair_single(ch, user, echo):
	print ('!!!!!!repairing', user)
	user = user.strip()
	u,domain = user.split('@')
	if ch.recv_ready():
		print("!!!!!!throwing", ch.recv(1024).decode("utf-8"))

	cmd = "imdbcontrol ga %s %s | grep Internal-ID | awk {'print $2'}\n" % (u, domain)
	ch.send(cmd)
	while not ch.recv_ready():
		time.sleep(0.5)
	uuid = ch.recv(1024).decode("utf-8")
	uuid = uuid.replace(echo, '')
	uuid = uuid.strip('\r\n')

	cmd = "imrecalcmboxstats  mx-mss01 repair %s '' > /dev/null 2>&1" % uuid
	print ('!!!!!!executing', cmd)
	ch.send(cmd + '\n')
	while not ch.recv_ready():
		time.sleep(0.5)
	print(ch.recv(1024).decode("utf-8"))


def single3_fail():
	try:
		ssh = SSHRunner()
		ssh.connect('root', '123456', '192.168.1.105')
		sh = ssh.shell2()

		i, o, e = sh.send("su - thundercumt")
		print(o.readlines())
		i, o, e = sh.send("whoami")
		print(o.readlines())
		i, o, e = sh.send("whoami")
		print(o.readline())

	except Exception as e:
		print(e)


if __name__ == "__main__":
	loop()