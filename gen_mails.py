#!/usr/bin/env python
# coding=utf-8

from time import ctime, sleep
from datetime import datetime
from queue import Empty
from multiprocessing.managers import BaseManager
import traceback

from api.mail import MailSMTP
from api.mail import MailIMAP
from api.selection import RoundRobinPolicy, DistributionPolicy, SamePolicy
from api.io import FileCache
from api.template import EmailTemplate, CategoryTemplate

from monitor.progress import Progress

from task_queue import get_task_queue

from config import smtp_servers, imap_servers
from config import template_list, template_dist, template_list_category
from config import account_list, account_prefix, account_postfix
from config import message_dist
from config import progress

class QueueManager(BaseManager):
	pass
QueueManager.register('get_task_queue', callable=get_task_queue)

p = Progress(**progress)

class EmailArgs(object):
	def __init__(self, sender, recipients, subject, special):
		self.sender = sender
		self.recipients = recipients
		self.subject = subject
		self.special = special

	def get_sender(self):
		return self.sender

	def get_recipients(self):
		return self.recipients

	def get_subject(self):
		return self.subject

	def get_special(self):
		return self.special


class MailGen(object):

	def __init__(self, host='127.0.0.1', port=5000, auth_key='guessWhat', id=None):
		self.host = host
		self.port = port
		self.auth_key = auth_key
		if id == None:
			import os
			id = str(os.getpid())
		self.id = id

	def start_master(self):
		manager = QueueManager(address=(self.host, self.port), authkey=self.auth_key)

		try:
			manager.start()
			manager.join()
		except Exception as e:
			print("damn something happened: ", e)
			traceback.print_exc()
			print('Quite the master!')

	def start_consumer(self):
		loader = FileCache()
		manager = QueueManager(address=(self.host, self.port), authkey=self.auth_key)
		manager.connect()
		task_q = manager.get_task_queue()

		smtp_selector = RoundRobinPolicy(smtp_servers)
		imap_selector = RoundRobinPolicy(imap_servers)
		template_selector = DistributionPolicy(template_list, template_dist)
		##template_selector = SamePolicy(['community.eml']);

		while True:
			try:
				account, num = task_q.get(timeout=10)
				print('starting to process %s with %d emails' % (account, num))
				host, port = smtp_selector.next()
				count = self.count_mailbox(imap_selector, account)

				#Since MTA may fail, we can only determine progress by IMAP
				p.report(account, 'mail', count, num)

				smtp = MailSMTP(host, port)
				smtp.connect()
				for i in range(count, num):
					template_name = template_selector.next()
					template_content = loader.load_file('resources/%s' % (template_name))
					email_args = EmailArgs(account, [account], 'subject:%d' % (i), datetime.now().strftime('%Y-%m-%d %H:%M'))

					# If category template is used, we can not specify sender and recipients
					# Only use Subject and Special in the template.
					if template_name in template_list_category:
						template = CategoryTemplate(template_content, email_args)
					else:
						template = EmailTemplate(template_content, email_args)

					print(ctime(), 'sending email to %s with title:%s' % (account, email_args.get_subject()))
					smtp.send_mail(account, [account], template.message())
					sleep(0.1)
				smtp.close()
			except Exception as e:
				if type(e) == Empty:
					continue
				else:
					print("damn something happened: ", e)
					traceback.print_exc()

	def start_supplier(self):
		manager = QueueManager(address=(self.host, self.port), authkey=self.auth_key)
		try:
			manager.connect()
			task_q = manager.get_task_queue()
			imap_selector = RoundRobinPolicy(imap_servers)

			accumulator = 1
			for idx in range(len(account_list)):

				batch_start = accumulator
				batch_scope = account_list[idx]
				batch_end = batch_start + batch_scope
				batch_message_count = message_dist[idx]

				for user in range(batch_start, batch_end):
					account = '%s%s%s' % (account_prefix, user, account_postfix)
					task = (account, batch_message_count)

					mail_progress = p.progress_user_category(account, 'mail')
					if mail_progress is None:
						p.report(account, 'mail', 0, batch_message_count)
					else:

						try:
							count_in_record = mail_progress['actual']
							count_expected = mail_progress['exp']

							if count_in_record >= count_expected and count_expected == batch_message_count:
								print('account %s: ok, recorded %d messages' % (account, count_in_record))
								continue

							count_actual = self.count_mailbox(imap_selector, account)

							print('checking %s: in_record: %d <> actual: %d' % (account, count_in_record, count_actual))
							if count_expected != batch_message_count or  count_actual != count_in_record:
								p.report(account, 'mail', count_actual, batch_message_count)

							if count_actual >= batch_message_count:
								continue

						except Exception as e:
							print("damn something happened: ", e)
							traceback.print_exc()

					task_q.put(task)
					print('created task: ', task)
					sleep(0.1)

				accumulator += batch_scope

		except Exception as e:
			print("damn something happened: ", e)
			traceback.print_exc()
			print('Quite the supplier!')

	def count_mailbox(self, imap_selector, account, pwd='p'):
		imap_host, imap_port = imap_selector.next()
		imap = MailIMAP(imap_host, imap_port)
		imap.login(account, pwd)
		count = imap.count_folder('inbox', read_only=True)

		if count is None:
			count = imap.count_folder('inbox', read_only=False)

		# if count is still None, I have done my best
		imap.logout()
		return count


if __name__ == '__main__':
	from sys import argv
	argc = len(argv)

	if argc < 5 or argc > 6:
		print("""
usage: python gen_mails.py <mode> <host> <port> <auth_key> [id]
where mode: master | supplier | slave
      id: string used to distinguish self
""")
		exit(1)

	mode = argv[1]
	argv_handler = {
		5: lambda : MailGen(host=argv[2], port=int(argv[3]), auth_key=bytes(argv[4], 'utf8')),
		6: lambda: MailGen(host=argv[2], port=int(argv[3]), auth_key=bytes(argv[4], 'utf8'), id=argv[5])
	}

	run = argv_handler[argc]()

	if mode == 'master':
		run.start_master()
	elif mode == 'supplier':
		run.start_supplier()
	else:
		run.start_consumer()
