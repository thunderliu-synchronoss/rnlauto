#!/usr/bin/env python
# coding=utf-8

from time import sleep
import traceback

from api.selection import RandomPolicy
from api.webtop2 import Webtop

from gen_users import get_users
from config import webtop_servers, contacts_per_account, sleep_time

from config import progress
from monitor.progress import Progress

users = get_users()
user_selector = RandomPolicy(users)
webtop_selector = RandomPolicy(webtop_servers)

p = Progress(**progress)

for u in users:
	print('checking user %s' % u)
	contact_progress = p.progress_user_category(u, 'contact')
	if contact_progress is None:
		p.report(u, 'contact', 0, contacts_per_account)

for u in users:
	print('working on user %s' % u)
	host, port, context = webtop_selector.next()
	w = Webtop(host, port, context)
	try:
		w.login(u)
		ad = w.get_main_address_book()
		existing_contacts = w.list_contacts(ad)
		contacts_count = existing_contacts['totalCount']

		for i in range(contacts_count, contacts_per_account):
			email = user_selector.next()
			first = 'f' + str(i)
			last = 'l' + str(i)
			c = w.create_contact(ad, first, last, email)
			print(c)
			sleep(sleep_time)
		p.report(u, 'contact', contacts_per_account, contacts_per_account)
	except Exception as e:
		print("damn something happened: %s" % e)
		traceback.print_exc()
