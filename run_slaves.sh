#!/bin/bash

for i in {1..10}
do
    nohup ./slave.sh  1>logs/slave${i}.log  2>&1 &
done