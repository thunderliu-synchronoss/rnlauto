#!/usr/bin/env python
# coding=utf-8

from gevent import monkey;

monkey.patch_all()

from time import sleep
import gevent
from api.user import UserDAO
from api.domain import DomainDAO

print("""
Although this is beneficial in theory, parts of created accounts have problems in a test.
I guess it may be caused by concurrent write to ldap server.
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!This is not expected to be used to create large amount of accounts.!!!!!!
!!!!!!Use are your own risk                                              !!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
""")

host = '10.48.129.59'
port = '8081'
domain = 'openwave.dom'
prefix = 'u'

def generate_users_by_num(prefix, num):
	user_list = []
	for idx in range(1, num + 1):
		user_name = prefix + str(idx)
		user_list.append(user_name + '@' + domain)
	return user_list

def generate_users_by_range(prefix, start, end):
	user_list = []
	for idx in range(start, end):
		user_name = prefix + str(idx)
		user_list.append(user_name + '@' + domain)
	return user_list


# create 1000 users with prefixing username load


def create_users(dao, user_list, idle=0.2):
	greenlets = []

	for user in user_list:
		if dao.check_user(user) is False:
			greenlets.append(
				gevent.spawn(UserDAO.create_user, dao, user)
			)
			#dao.create_user(user)
	gevent.joinall(greenlets)

def create_domain(dao, domain_list, idle=0.2):
	for domain in domain_list:
		if dao.check_domain(domain) is False:
			dao.create_domain(domain)
			if idle > 0:
				sleep(idle)

domain_dao = DomainDAO(host, port)
user_dao = UserDAO(host, port)
user_list = generate_users_by_num(prefix, 10)
create_domain(domain_dao, [domain])
create_users(user_dao, user_list)



"""
def f(url):
	print('GET: %s' % url)
	try:
		resp = urllib2.urlopen(url)
		data = resp.read()
		print('%d bytes received from %s.' % (len(data), url))
	except urllib2.URLError as e:
		print 'http request failed. Reason: %s' % (e.reason)


gevent.joinall([
	gevent.spawn(f, 'http://10.48.129.59:8081/mxos/domain/v2/openwave.dom'),
	gevent.spawn(f, 'http://10.48.129.59:8081/mxos/domain/v2/rnl.dom'),
	gevent.spawn(f, 'http://10.48.129.59:8081/mxos/domain/v2/synchronoss.com'),
])
"""