#!/usr/bin/env python
# coding=utf-8

import time
from config import remote_jobs
from monitor.remote import RemoteTaskExecutor, SSHRunner
from core.reflection import import_string


def do_monitor(config):
	"""
	:param config: the remote_tasks defined in config.py 
	:return: None, it routinely triggers tasks 
	
	config: {
		"defaults": {},
		"tasks": []
	}
	
	"defaults": {
		"conn_user": "root",
		"conn_pwd": "Synchron0ss",
		"task_user": "imail",
		"frequency_seconds": 60 * 60
	},
	
	"jobs":[
		{
			'desc': 'free mail servers',
			'hosts': ['mx-mss01', 'mx-mss02'],
			'tasks': [
				{
					'task_type': 'remote.FileTruncate',
					'work_dirs': ['/opt/imail/log'],
					'args': '*'
				},
			]
		}
	] 
	"""

	defaults = config['defaults']
	jobs = config['jobs']
	frequency = defaults['frequency_seconds']

	while True:
		for job in jobs:
			handle_job(defaults, job)
		time.sleep(frequency)


def handle_job(defaults, job):
	"""
	:param defaults:
			"conn_user": "root",
			"conn_pwd": "Synchron0ss",
			"task_user": "imail",
			"frequency_seconds": 60 * 60
	:param job: 
			'desc': 'free mail servers',
			'hosts': ['mx-mss01', 'mx-mss02'],
			'tasks': [
				{
					'task_type': 'remote.FileTruncate',
					'work_dirs': ['/opt/imail/log'],
					'args': '*'
				},
			]
	:return: None
	"""

	conn_user = defaults['conn_user']
	pwd = defaults['conn_pwd']
	task_user = defaults['task_user']

	hosts = job['hosts']
	tasks = job['tasks']
	for host in hosts:
		with RemoteTaskExecutor(SSHRunner, host, conn_user, pwd) as remote:
			for task in tasks:
				task_type = task['task_type']
				kwargs = {}
				if 'task_arguments' in task:
					kwargs = task['task_arguments']
				cls = import_string(task_type)
				task_instance = cls(**kwargs) # must be **kwargs to map to named args in functions

				remote.execute_task(task_instance)


if __name__ == '__main__':
	do_monitor(remote_jobs)
