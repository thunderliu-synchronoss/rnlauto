#!/usr/bin/env python
# coding=utf-8

from time import sleep
from api.user import UserDAO
from api.domain import DomainDAO
from api.selection import RandomPolicy

from config import account_prefix, account_postfix, account_list
from config import mxos_servers

mxos_selector = RandomPolicy(mxos_servers)

def generate_users_by_num(prefix, num, at_domain):
	user_list = []
	for idx in range(1, num + 1):
		user_name = prefix + str(idx)
		user_list.append(user_name + at_domain)
	return user_list

def generate_users_by_range(prefix, start, end, at_domain):
	user_list = []
	for idx in range(start, end + 1):
		user_name = prefix + str(idx)
		user_list.append(user_name  + at_domain)
	return user_list


# create 1000 users with prefixing username load


def create_users(dao, user_list, idle=0.2):
	for user in user_list:
		if dao.check_user(user) is False:
			dao.create_user(user)
			if idle > 0:
				sleep(idle)

def create_domain(dao, domain_list, idle=0.2):
	for domain in domain_list:
		if dao.check_domain(domain) is False:
			dao.create_domain(domain)
			if idle > 0:
				sleep(idle)


def get_domain():
	return account_postfix[1:]

def get_users_iter():
	accumulator = 1
	for idx in range(len(account_list)):
		batch_start = accumulator
		batch_scope = account_list[idx]
		batch_end = batch_start + batch_scope

		for user in range(batch_start, batch_end):
			account = '%s%s%s' % (account_prefix, user, account_postfix)
			yield account

		accumulator += batch_scope


def get_users():
	l = []
	accumulator = 1
	for idx in range(len(account_list)):
		batch_start = accumulator
		batch_scope = account_list[idx]
		batch_end = batch_start + batch_scope

		for user in range(batch_start, batch_end):
			account = '%s%s%s' % (account_prefix, user, account_postfix)
			l.append(account)

		accumulator += batch_scope

	return l


if __name__ == '__main__':
	(host, port) = mxos_selector.next()
	domain_dao = DomainDAO(host, port)
	user_dao = UserDAO(host, port)
	domain = get_domain()
	#create_domain(domain_dao, [domain])
	users = get_users()
	print(users)
	create_users(user_dao, users)
