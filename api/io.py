#!/usr/bin/env python
# coding=utf-8

class FileCache(object):

	def __init__(self):
		self.content_map = {}

	"""
	No locking mechanism, each slave will read its own template once
	"""
	def load_file(self, file_path):
		if file_path in self.content_map:
			return self.content_map[file_path]
		else:
			with open(file_path) as fd:
				contents = fd.read()
			self.content_map[file_path] = contents
			return contents