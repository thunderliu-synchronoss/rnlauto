#!/usr/bin/env python
# coding=utf-8

import requests
import json

from api.mxos import MXOS

class DomainDAO(MXOS):
	domain_path = '/domain/v2/'

	default_create_args = {
		'type': 'local'
	}

	def __init__(self, host, port):
		MXOS.__init__(self, host, port)
		self.url = '%s%s' % (MXOS.mxosUrl(self), DomainDAO.domain_path)

	def create_domain(self, domain):
		uri = self.url + domain;

		payload = DomainDAO.default_create_args.copy()
		r = requests.put(uri, data=payload)
		if r.status_code == 200:
			print('created domain %s' % (domain))
		else:
			print('create domain %s FAILED! Reason: %s' % (domain, r.text))

	def check_domain(self, domain):
		uri = self.url + domain
		r = requests.get(uri)
		res = json.loads(r.text)
		if 'domain' in res:
			print('domain %s existed' % (domain))
			return True
		else:
			print('domain %s does NOT exist. Reason: %s' % (domain, r.text))
			return False
