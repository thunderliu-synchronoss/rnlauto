#!/usr/bin/env python
# coding=utf-8

from api.base import Base

import smtplib
import imaplib

class MailSMTP(Base):
	def __init__(self, host, port):
		Base.__init__(self, host, port)


	def send_single_mail_content(self, sender, rcpt_list, subject, body):

		#indentation matters, the following message should guarantee texts are aligned left.
		#Otherwise, it will be considered empty mail without subject
		message = """\
From: %s
To: %s
Subject: %s

%s
""" % (sender, ", ".join(rcpt_list), subject, body)

		print(message)
		# Send the mail
		server = smtplib.SMTP(self.host, int(self.port))
		server.sendmail(sender, rcpt_list, message)
		server.quit()

	def send_single_mail(self, sender, rcpt_list, message):
		server = smtplib.SMTP(self.host, int(self.port))
		server.sendmail(sender, rcpt_list, message)
		server.quit()

	def connect(self):
		self.server = smtplib.SMTP(self.host, int(self.port))

	def send_mail_content(self, sender, rcpt_list, subject, body):
		message = """\
From: %s
To: %s
Subject: %s

%s
		""" % (sender, ", ".join(rcpt_list), subject, body)

		# Send the mail
		self.server.sendmail(sender, rcpt_list, message)

	def check_encoding(self, message):
		for ch in message:
			if ord(ch) < 128:
				print(ord(ch), ch)

	def send_mail(self, sender, rcpt_list, message):
		self.server.sendmail(sender, rcpt_list, message)

	def close(self):
		self.server.quit()

class MailIMAP(Base):
	def __init__(self, host, port):
		Base.__init__(self, host, port)

	def login(self, account, pwd):
		self.conn = imaplib.IMAP4(self.host, self.port)
		self.conn.login(account, pwd)

	def logout(self):
		self.conn.logout()

	def list_folder(self):
		return self.conn.list()

	def select_folder(self, folder):
		self.conn.select(folder, False)

	def create_folder(self, folder):
		self.folder = folder
		self.conn.create(folder)

	def close_folder(self):
		if self.folder is not None:
			self.conn.close()
			self.folder = None

	def list_all_uids(self):
		res = self.conn.uid('search', 'all')
		if res[0] == 'OK':
			return ('OK', res[1][0].split())
		return ('Error', [])

	def list_all_nums(self):
		return self.conn.search('utf-8', 'all')

	def count_current_folder(self):
		status, uids = self.list_all_uids()
		return len(uids)

	def count_folder(self, folder, read_only=True):
		typ, data = self.conn.select(folder, read_only)
		if typ == 'OK':
			return int(data[0])
		return None

	def top_uid(self):
		uids = self.list_all_uids()
		if uids[0] == 'OK' and len(uids[1]) > 0:
			return ('OK', uids[1][-1])
		return ('ERROR', None)

	def move_to_folder(self, uid, folder):
		status, data = self.conn.uid('COPY', uid, folder)
		if (status == 'OK'):
			self.conn.uid('STORE', uid, '+FLAGS', '(\Deleted)')
			self.conn.expunge()
		return ('OK', )


"""
r = MailIMAP('10.37.5.102', 11143)
r.login('u9@openwave.dom', 'p')
r.count_folder('Drafts')
r = MailIMAP('10.37.5.102', 11143)
r.login('u9@openwave.dom', 'p')
print(r.list_folder())

r.select_folder('inbox')
print(r.list_all_uids())

r.move_to_folder(1008, 'Sent')

r.select_folder('Sent')
print(r.list_all_uids())

print(r.top_uid())

r.logout()
"""



#send = MailSMTP('10.48.129.39', 2525)
#from api.io import FileCache
#c = FileCache()
#send.send_single_mail('u20@openwave.dom', ['u20@openwave.dom'], c.load_file('../resources/5k.eml'))
#send.send_single_mail('not.exist@openwave.corn', ['u20@openwave.dom', 'u20@openwave.dom'], 'test', 'hello world guys')

#send.connect()
#send.send_mail('n.a@na.com', ['u9@openwave.dom', 'u10@openwave.dom', 'u8@openwave.dom'], 'hello world', 'guys come to play1')
#send.send_mail('n.a@na.com', ['u9@openwave.dom', 'u10@openwave.dom', 'u8@openwave.dom'], 'hello world', 'guys come to play2')
#send.close()
