#!/usr/bin/env python
# coding=utf-8

import abc
from random import randint

__all__ = ['RoundRobinPolicy', 'RandomPolicy', 'DistributionPolicy']

class SelectionPolicy(object):
	def __init__(self, data):
		self.data = data

	@abc.abstractmethod
	def next(self):
		pass

class SamePolicy(SelectionPolicy):
	def __init__(self, data):
		SelectionPolicy.__init__(self, data)
		self.selected = data[0]

	def next(self):
		return self.selected

class RoundRobinPolicy(SelectionPolicy):
	def __init__(self, data):
		self.i = 0
		self.n = len(data)
		SelectionPolicy.__init__(self, data)

	def next(self):
		i = self.i
		self.i += 1
		self.i %= self.n
		return self.data[i]


class RandomPolicy(SelectionPolicy):
	def __init__(self, data):
		n = len(data)
		if n > 0:
			self.n = n - 1
		else:
			self.n = 0
		SelectionPolicy.__init__(self, data)

	def next(self):
		i = randint(0, self.n)
		return self.data[i]

class DistributionPolicy(SelectionPolicy):
	def __init__(self, data, dist_list):
		self.distribution = dist_list
		SelectionPolicy.__init__(self, data)

	def next(self):
		start = 0
		r = randint(1, sum(self.distribution))
		for index, item in enumerate(self.distribution):
			start += item
			if r <= start:
				break
		return self.data[index]