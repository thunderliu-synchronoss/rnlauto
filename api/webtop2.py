#!/usr/bin/env python
# coding=utf-8

import requests
import json
import time

from api.base import Base

WEBTOP_CSRF_TOKEN='X-Webtop-CSRF-Token'
WEBTOP_SESSION_ID='webtop_sessionId_v1'
WEBTOP_AUTH_TOKEN='webtop_authToken_v1'

class WebtopCookie(object):
	def __init__(self, session_id=None, auth_token=None):
		self.session_id = session_id
		self.auth_token = auth_token

	def to_dict(self):
		t = dict()
		if self.session_id is not None:
			t[WEBTOP_SESSION_ID] = self.session_id
		if self.auth_token is not None:
			t[WEBTOP_AUTH_TOKEN] = self.auth_token
		return t


class Webtop(Base):

	def __init__(self, host, port, context):
		Base.__init__(self, host, port)
		self.context = context
		self.user = None
		self.webtop_cookie = None
		self.csrfToken = None

	def context(self):
		return self.context

	def _base_url(self):
		return 'http://%s:%d/%s' % (self.host, self.port, self.context)

	def login_url(self):
		return '%s/bin/auth' % (self._base_url())

	def json_url(self):
		return '%s/json' % (self._base_url())

	def do_webtop_request(self, url, webtop_method, dictionary):
		data = json.dumps(dictionary)
		data = 'r=%s'% (webtop_method) + data

		headers = {'Content-Type': 'application/x-www-form-urlencoded'}
		if self.csrfToken is None:
			r = requests.post(url, data=data, headers=headers)
		else:
			headers[WEBTOP_CSRF_TOKEN] = self.csrfToken
			cookie = self.webtop_cookie.to_dict()
			r = requests.post(url, data=data, headers=headers, cookies=cookie)
		return r

	def parse_webtop_response(self, r):
		contents = r.text
		cookies = r.cookies

		if len(cookies) > 0 and WEBTOP_SESSION_ID in cookies:
			session_id = cookies[WEBTOP_SESSION_ID]
			auth_token = cookies[WEBTOP_AUTH_TOKEN]
			self.webtop_cookie = WebtopCookie(session_id, auth_token)

		json_contents = json.loads(contents)
		return json_contents

	def call(self, url, method, json):
		try:
			r = self.do_webtop_request(url, method, json)
			res = self.parse_webtop_response(r)
			return res['result']
		except Exception as e:
			print ('%s %s failed. Reason: %s' % (self.user, method, e))


	def login(self, user, pwd='p'):
		url = self.login_url()
		json = {"username": user, "password": pwd, "csrfProtection": True}
		res = self.call(url, 'auth.login', json)
		self.csrfToken = res['csrfToken']
		self.user = user


	def list_calendars(self):
		url = self.json_url()
		return self.call(url, 'calendar.list', {})

	def get_main_calendar(self):
		calendars = self.list_calendars()
		if calendars is not None:
			for cal in calendars:
				if cal['name'].upper().endswith('MAIN CALENDAR'):
					return cal
		return None

	def list_task_lists(self, calendar):
		json = {
			'calendarId': calendar['id']
		}
		return self.call(self.json_url(), 'calendar.taskList.list', json)

	def create_task(self, calendar, summary, status='IN-PROCESS'):
		todo = {
			"@type": "ToDo",#this is a must
			'calendarId': calendar['id'],
			'summary': summary,
			'status': status
		}
		json = {
			'toDo': todo
		}
		return self.call(self.json_url(), 'calendar.toDo.create', json)

	def list_tasks(self, calendar):
		json = {
			'calendarId': calendar['id'],
		}
		return self.call(self.json_url(), 'calendar.toDo.report', json)

	def convert_to_millies(self, date_time):
		t = time.strptime(date_time, '%Y%m%dT%H%M%S')
		seconds = time.mktime(t)
		return int(seconds) * 1000

	def create_event(self, calendar, summary, start_time, end_time):

		event = {
			"@type": "Event",#this is a must
			'calendarId': calendar['id'],
			'summary': summary,
			'startMillis': self.convert_to_millies(start_time),
			'endMillis': self.convert_to_millies(end_time),
			'attendees': []
		}
		json = {
			'event': event
		}
		return self.call(self.json_url(), 'calendar.event.create', json)

	def list_events(self, calendar, start_time, end_time):
		json = {
			'calendarIds': [calendar['id']],
			'startTime': start_time,
			'endTime': end_time
		}
		return self.call(self.json_url(), 'calendar.event.report', json)

	def list_address_books(self):
		url = self.json_url()
		r = self.call(url, 'contacts.addressBook.list', {})
		return r

	def get_main_address_book(self):
		adds = self.list_address_books()
		if adds is not None:
			for ad_book in adds:
				if ad_book['name'].upper() == 'MAIN':
					return ad_book
		return None

	def list_contacts(self, address_book):
		json = {
			'addressBookId': address_book['id'],
		}
		return self.call(self.json_url(), 'contacts.list', json)

	def create_contact(self, address_book, first_name, last_name, email):
		email_fields = [{"label": "home", "type": "lzEmail", "value": email, "values": [], "primary": True}]
		contact = {
			'@type': 'Contact',
			'firstName': first_name,
			'lastName': last_name,
			'fields': email_fields
		}
		json = {
			'addressBookId': address_book['id'],
			'contact': contact
		}
		r = self.call(self.json_url(), 'contacts.createContact', json)
		return r


if __name__ == '__main__':
	w = Webtop('10.37.5.102', 8080, 'local-mx')
	w.login('u11@openwave.dom', 'p')
	cal = w.get_main_calendar()
	print(w.list_events(cal, '20180101T000000', '20180131T000000'))