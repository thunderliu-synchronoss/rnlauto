#!/usr/bin/env python
# coding=utf-8

from api.base import Base

class MXOS(Base):
	def __init__(self, host, port):
		Base.__init__(self, host, port)

	def mxosUrl(self):
		return 'http://%s:%d/mxos' % (self.host, self.port)