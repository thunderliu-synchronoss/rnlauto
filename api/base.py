#!/usr/bin/env python
# coding=utf-8

class Base(object):
	host = ''
	port = 0

	def __init__(self, host, port):
		self.host = host
		self.port = port

	def host(self):
		return self.host

	def port(self):
		return self.port