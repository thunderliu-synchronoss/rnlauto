#!/usr/bin/env python
# coding=utf-8

import requests
import json

from api.mxos import MXOS

class UserDAO(MXOS):
	USER_PATH = '/mailbox/v2/'

	default_create_args = {
		'cosId': 'default',
		'maxNumAliases': 3,
		'senderBlockingEnabled': 'yes',
		'senderBlockingAllowed': 'yes',
		'sieveFilteringEnabled': 'yes'
	}

	def __init__(self, host, port):
		MXOS.__init__(self, host, port)
		self.url = '%s%s' % (MXOS.mxosUrl(self), UserDAO.USER_PATH)

	def create_user(self, user_name, password='p'):
		uri = self.url + user_name
		payload = UserDAO.default_create_args.copy()
		payload['password'] = password
		r = requests.put(uri, data=payload)

		if r.status_code == 200:
			print('created user %s' % (user_name))
		else:
			print('create user %s failed. Reason: %s' % (user_name, r.text))

	def check_user(self, user_name):
		uri = self.url + user_name
		r = requests.get(uri)
		result = json.loads(r.text)

		if 'base' in result:
			print('user %s existed' % (user_name))
			return True
		else:
			print('user %s does NOT exist' % (user_name))
			return False

	def delete_user(self, user_name):
		pass
