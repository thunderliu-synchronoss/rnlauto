#!/usr/bin/env python
# coding=utf-8

class EmailTemplate(object):
	def __init__(self, template_content, template_args):
		self.template_content = template_content
		self.template_args = template_args

	def sender(self):
		return self.template_args.get_sender()

	def subject(self):
		return self.template_args.get_subject()

	def recipients(self):
		return ", ".join(self.template_args.get_recipients())

	def message(self):
		a = self.sender()
		b = self.recipients()
		c = self.subject()
		d = self.template_args.get_special()
		return self.template_content % (a,b,c,d)

class CategoryTemplate(EmailTemplate):
	def __init__(self, template_content, template_args):
		EmailTemplate.__init__(self, template_content, template_args)

	def sender(self):
		return ''

	def subject(self):
		return self.template_args.get_subject()

	def recipients(self):
		return ''

	def message(self):
		c = self.subject()
		d = self.template_args.get_special()
		return self.template_content % (c,d)
