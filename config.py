#!/usr/bin/env python
# coding=utf-8

sleep_time = 0.5

webtop_servers = [('10.48.129.45', 8080, 'kiwi'), ('10.48.129.45', 8080, 'kiwi')]
smtp_servers = [('10.48.129.38', 2525), ('10.48.129.39', 2525)]
imap_servers = [('10.48.129.42', 143), ('10.48.129.43', 143)]
mxos_servers = [('10.48.129.58', 8081), ('10.48.129.59', 8081)]

template_list_category = ['5k.eml', '12k.eml'] #special template used for category
template_list = ['5k.eml', '12k.eml', '25k.eml', '50k.eml', '100k.eml', '150k.eml', '175k-image.eml', '185k-docs.eml', '200k-pdf.eml']
template_dist = [15, 15, 15, 15, 15, 10, 5, 5, 5]

account_prefix = 'x'
account_postfix = '@rnl.dom'
account_list = [3000, 3000, 3000, 3000, 3000,     500,  500,  500,  500,  500,      200,  200,  200,  200,   200,      200,  200,  200,   200,   200]
message_dist = [300,  600,  1200, 1800, 2100,     1000, 2000, 4000, 6000, 7000,     2000, 4000, 8000, 12000, 14000,    3500, 7000, 14000, 21000, 24500]

contacts_per_account = 200

tasks_per_account = 50
events_per_account = 20
events_since = '20180129T083000'
event_duration_seconds = 2 * 60 * 60
events_interval_seconds = 2 * 24 * 60 * 60


progress = {
	'host': '10.37.5.102',
	'port': 27017
}


remote_jobs = {
	"repair": {
		'host': '10.48.129.54'
	},
	"defaults": {
		"conn_user": "root",
		"conn_pwd": "Synchron0ss",
		"task_user": "imail",
		"frequency_seconds": 60 * 60
	},
	"jobs": [
		{
			'desc': 'check disk usage',
			'hosts': ['10.37.5.102'],
			'tasks': [
				{
					'task_type': 'monitor.remote.DiskUsage',
				},
			]
		},
{
			'desc': 'ls -l',
			'hosts': ['10.37.5.102'],
			'tasks': [
				{
					'task_type': 'monitor.remote.AdHocCmd',
					'task_arguments': {
						'cmd': 'ls -l'
					}
				},
			]
		}
	]
}

"""
remote_jobs = {
	"defaults": {
		"conn_user": "root",
		"conn_pwd": "Synchron0ss",
		"task_user": "imail",
		"frequency_seconds": 60 * 60
	},
	"jobs": [
		{
			'desc': 'free razorgate space',
			'hosts': ['mx-razgate01', 'mx-razgate02'],
			'tasks': [
				{
					'task_type': 'monitor.remote.FileTruncate',
					'task_arguments': {
						'work_dirs': ['/opt/data/rg/mira/usr/store/log'],
						'args': '*'
					}
				},
				{
					'task_type': 'monitor.remote.FileClean',
					'task_arguments': {
						'work_dirs': ['/opt/data/rg/mira/usr/store/rules/vadeas_backups'],
						'args': '*'
					}
				},
			]
		},
		{
			'desc': 'free mail servers',
			'hosts': ['mx-mail01', 'mx-mail02'],
			'tasks': [
				{
					'task_type': 'monitor.remote.FileTruncate',
					'task_arguments': {
						'work_dirs': ['/opt/imail/log'],
						'args': '*'
					}
				},
				{
					'task_type': 'monitor.remote.FileClean',
					'task_arguments': {
						'work_dirs': ['/opt/imail/spool/messages'],
						'command': '*'
					}
				},
			]
		},
		{
			'desc': 'free mail servers',
			'hosts': ['mx-mss01', 'mx-mss02'],
			'tasks': [
				{
					'task_type': 'monitor.remote.FileTruncate',
					'task_arguments': {
						'work_dirs': ['/opt/imail/log'],
						'args': '*'
					}
				},
			]
		},
		{
			'desc': 'monitor disk usage',
			'hosts': ['mx-razgate01', 'mx-razgate02', 'mx-mail01', 'mx-mail02'],
			'tasks': [
				{
					'task_type': 'monitor.remote.DiskUsage',
				}
			]
		}
	]
}
"""