#!/usr/bin/env python
# coding=utf-8

from time import sleep, strptime
import datetime

from api.selection import RandomPolicy
from api.webtop2 import Webtop

from config import webtop_servers, tasks_per_account, events_per_account, \
	events_since, events_interval_seconds, event_duration_seconds, sleep_time
from gen_users import get_users

from config import progress
from monitor.progress import Progress

webtop_selector = RandomPolicy(webtop_servers)
users = get_users()
user_selector = RandomPolicy(users)

p = Progress(**progress)

def next_timestamp(webtop_timestamp, secs_delta):
	t = datetime.datetime.strptime(webtop_timestamp, '%Y%m%dT%H%M%S')
	t += datetime.timedelta(seconds=secs_delta)
	return datetime.datetime.strftime(t, '%Y%m%dT%H%M%S')

def events_until(since_timestamp, interval, count, duration):
	t = datetime.datetime.strptime(since_timestamp, '%Y%m%dT%H%M%S')
	t += datetime.timedelta(seconds=interval * count + duration)
	return datetime.datetime.strftime(t, '%Y%m%dT%H%M%S')

for u in users:
	print('checking user %s' % u)
	task_progress = p.progress_user_category(u, 'task')
	if task_progress is None:
		p.report(u, 'task', 0, tasks_per_account)

	event_progress = p.progress_user_category(u, 'event')
	if event_progress is None:
		p.report(u, 'event', 0, events_per_account)

for u in users:
	print('working on user %s' % u)
	host, port, context = webtop_selector.next()
	w = Webtop(host, port, context)
	w.login(u)
	cal = w.get_main_calendar()

	existing_tasks = w.list_tasks(cal)
	task_count = existing_tasks['toDos']['totalCount']
	p.report(u, 'task', task_count, tasks_per_account)

	for i in range(task_count, tasks_per_account):
		summary = 'task summary ' + str(i)
		e = w.create_task(cal, summary)
		print(e)
		sleep(sleep_time)
	p.report(u, 'task', tasks_per_account, tasks_per_account)


	deadline = events_until(events_since, events_interval_seconds,
							events_per_account, event_duration_seconds)
	existing_events = w.list_events(cal, events_since, deadline)
	last_start = events_since
	total_events = existing_events['events']['totalCount']
	p.report(u, 'event', total_events, events_per_account)

	if total_events > 0:
		last_start = existing_events['events']['results'][-1]['startTime']

	for i in range(total_events, events_per_account):
		summary = 'event summary ' + str(i)
		last_end = next_timestamp(last_start, event_duration_seconds)
		e = w.create_event(cal, summary, last_start, last_end)
		print(e)
		last_start = next_timestamp(last_start, events_interval_seconds)
		sleep(sleep_time)
	p.report(u, 'event', events_per_account, events_per_account)