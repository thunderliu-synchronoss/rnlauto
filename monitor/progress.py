#!/usr/bin/env python
# coding=utf-8

from pymongo import MongoClient


class Progress(object):

	def __init__(self, host, port):
		self.host = host
		self.port = port
		self.client = MongoClient(host, port)
		self.db = self.client.progress_db
		self.table = self.db.progress

	def report(self, user, category, num, expects):
		self.table.update(
			{'_id': user},
			{'$set': {
				category: {
					'actual': num,
					'exp': expects
			}}},
			upsert=True
		)

	def progress_user(self, user):
		return self.table.find_one({'_id': user})

	def progress_user_category(self, user, category):
		obj = self.table.find_one({'_id': user})
		if obj is not None and category in obj:
			return obj[category]
		return None

	def progress_category(self, category):
		category_exp = '$%s.exp' % category
		category_actual = '$%s.actual' % category
		return self.table.aggregate([
			# override if actual value is gt exp value
			{'$project': { '%s.exp' % category: 1, '%s.actual' % category: {'$min': [category_exp, category_actual]}}},

			# '$group': {'_id': '$mail.exp', 'actual': {'$sum': '$mail.actual'}, 'exp': {'$sum': '$mail.exp'}}}
			{'$group': {'_id': category_exp, 'actual': {'$sum': category_actual}, 'exp': {'$sum': category_exp}}}
		])

	def progress_all(self):
		categories = ['mail', 'contact', 'event', 'task']
		for c in categories:
			print('calculating progress of %s:' % c)
			pro = self.progress_category(c)
			act_sum, exp_sum = 0, 0

			for x in pro:
				print(x)
				act_sum = act_sum + x['actual']
				exp_sum = exp_sum + x['exp']

			if exp_sum != 0:
				print('progress of %s is %f (%d of %d)\n' % (c, act_sum / exp_sum, act_sum, exp_sum))
			else:
				print('progress of %s is 0 (%d of %d)\n' % (c, act_sum, exp_sum))

		return self.table.aggregate([
			{'$addFields': {
				'mail_exp': '$mail.exp',
				'mail_actual': '$mail.actual',
				'event_exp': '$event.exp',
				'event_actual': '$event.actual',
				'task_exp': '$task.exp',
				'task_actual': '$task.actual',
				'contact_exp': '$contact.exp',
				'contact_actual': '$contact.actual'
			}},

			{'$group': {'_id': '$mail_exp', 'actual': {'$sum': '$mail_actual'}, 'exp': {'$sum': '$mail_exp'}}}
		])

	def _truncate(self):
		self.table.remove()

