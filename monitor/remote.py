#!/usr/bin/env python
# coding=utf-8

import paramiko
from io import StringIO
import time

class Shell(object):
	def __init__(self, channel):
		self.channel = channel

	def __enter__(self):
		# this is a must
		return self

	def send(self, cmd):
		i = StringIO(cmd)

		self.channel.send("%s%s" % (cmd, "\n"))
		while not self.channel.recv_ready():
			time.sleep(0.5)
		response = self.channel.recv(1024).decode("utf-8")
		o = StringIO(response)
		e = StringIO()

		return i, o, e

class SSHRunner(object):
	def __init__(self,):
		self.ssh = paramiko.SSHClient()
		self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy)

	def connect(self, user, pwd, host, port=22):
		self.ssh.connect(hostname=host, port=port, username=user, password=pwd)

	def run(self, cmd):
		return self.ssh.exec_command(cmd, get_pty=True)

	def shell(self):
		channel = self.ssh.invoke_shell()
		return channel

	def shell2(self):
		channel = self.ssh.invoke_shell()
		return Shell(channel)

	def close(self):
		self.ssh.close()


class RemoteTask(object):

	def commands(self):
		pass


class RemoteTaskExecutor(object):

	def __init__(self, remote_runner_cls, host, user, pwd):
		self.host = host
		self.user = user
		self.pwd = pwd

		if remote_runner_cls != SSHRunner:
			raise NotImplementedError()
		self.ssh = remote_runner_cls()

	def __enter__(self):
		self.ssh.connect(self.user, self.pwd, self.host)
		print(self.host)
		# this is a must
		return self

	def execute_task(self, remote_task):
		for cmd in remote_task.commands():
			print(remote_task.__class__.__name__, cmd)
			i, o, e = self.ssh.run(cmd)
			for line in o.readlines():
				print(line.strip())

	def __exit__(self, exc_type, exc_val, exc_tb):
		self.ssh.close()
		print()


class AdHocCmd(RemoteTask):
	def __init__(self, cmd):
		self.cmd = cmd

	def commands(self):
		return [self.cmd]


class FileClean(RemoteTask):
	def __init__(self, work_dirs=[], args='*'):
		self.paths = work_dirs
		self.reg = args

	def commands(self):
		for path in self.paths:
			yield "rm -rf %s/%s" % (path, self.reg)


class FileTruncate(RemoteTask):
	def __init__(self, work_dirs=[], args='*'):
		self.paths = work_dirs
		self.reg = args

	def commands(self):
		for path in self.paths:
			yield "truncate -s 0 %s/%s" % (path, self.reg)


class DiskUsage(RemoteTask):
	def __init__(self):
		pass

	def commands(self):
		return ["df -h | awk '/\/$/ {print $5}'"]

